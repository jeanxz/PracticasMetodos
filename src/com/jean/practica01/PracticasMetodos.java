package com.jean.practica01;

import java.util.ArrayList;
import java.util.List;

import jdk.nashorn.internal.ir.BreakableNode;

public class PracticasMetodos {

	public int sumarLista(List<Integer> miLista) {
		int nu = 0;
		for (int i = 0; i < miLista.size(); i++) {

			nu = nu + miLista.get(i);

		}
		return nu;
	}

	public boolean miListaTruOFalse(List<Integer> lista) {

		int a = 0, b = 0;

		for (int i = 0; i < lista.size(); i++) {

			lista.get(i);

			if (lista.get(i) == 5) {
				a = 5;
			}

			if (lista.get(i) == 7) {
				b = 7;
			}

		}
		if (a == 5 && b == 7) {
			return true;
		} else {
			return false;
		}

	}

	public int miListaMayor13(List<Integer> listado) {

		int cont = 0;

		for (int i = 0; i < listado.size(); i++) {

			cont = cont + listado.get(i);

		}

		if (cont < 13) {

			return cont;
		} else {
			return 13;
		}

	}

	public boolean miListaNumerosPares(List<Integer> lista) {
		int cont = 0;
		int datosArray;
		int acumulador = 0;
		List<Integer> contenedor = new ArrayList<Integer>();
		for (int i = 0; i < lista.size(); i++) {

			// System.out.println(cont = lista.get(i)%2);
			datosArray = (cont = lista.get(i) % 2);

			contenedor.add(datosArray);

			contenedor.get(i);

			if (contenedor.get(i) == 0) {

				acumulador = acumulador + 1;

			}
		}

		if (acumulador >= 3) {
			return true;
		} else {
			return false;
		}

	}

	public int vecesRepetidoDeNumeroEnLista(List<Integer>miLista,int numero){
			
		int vecesRespetidas=0;
		for(int i=0;i<miLista.size(); i++){
				
			//System.out.println(miLista.get(i));
			
			
			if(miLista.get(i)==numero){
				vecesRespetidas= vecesRespetidas+1;
			}
			
		}
		return vecesRespetidas;
	}

	public Integer listaNombres(List<String> nombres,String nombre){
		 String nombrec="";
		int numCaracteres=0;
		for(int i=0;i<nombres.size();i++){
			
			
			nombrec = nombres.get(i);
			
			if(nombre.equals(nombrec)){
				
				 numCaracteres= nombrec.length();
				 return numCaracteres;
				
			}else{
				
				numCaracteres=0;
			}
			
		}
		
		
			return numCaracteres;
			
		
	}
}

