package com.jean.practica01;

import java.util.ArrayList;
import java.util.List;

public class MiMain {

	public static void main(String[] args) {
		
		List<Integer> listaNumeros = new ArrayList<Integer>();
		listaNumeros.add(7);
		listaNumeros.add(2);
		listaNumeros.add(3);
		listaNumeros.add(4);
		listaNumeros.add(3);
		listaNumeros.add(7);
		listaNumeros.add(7);
		
		
		List<String> listaNombres = new ArrayList<String>();
		
		listaNombres.add("Edwin");
		listaNombres.add("Yondry");
		listaNombres.add("Cindy");
		listaNombres.add("Jean");
		listaNombres.add("Pedro");
		
		
		
		PracticasMetodos miListado = new PracticasMetodos();
		
		//System.out.println(miListado.sumarLista(listaNumeros));
		
		//System.out.println(miListado.vecesRepetidoDeNumeroEnLista(listaNumeros, 7));
		
	System.out.println(miListado.listaNombres(listaNombres, new String("Jean")));
	}

}
